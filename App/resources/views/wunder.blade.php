<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Wunder</title>

        <!-- CSS Files -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <link href="css/paper-bootstrap-wizard.css" rel="stylesheet" />

        <!-- Fonts and Icons -->
        <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
        <link href="css/themify-icons.css" rel="stylesheet">

        <style>
            html, body {
                height: 100%;
                background-color: #f2f2f2;
            }
        </style>

    </head>
    <body>

        <div class="container h-100 d-flex justify-content-center">
            <div class="my-auto">

                <div class="picture-container">
                    <div class="picture">
                        <img src="img/wunder_logo_transp.png" width="100" height="100" class="picture-src" title="" />
                    </div>
                </div>

                <h1 class="display-3">Wunderfleet</h1>
                @if (!isset($data))
                <h1 class="display-4">Hello friend, let's record some data!</h1>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (isset($data))
                    <!-- <div class="alert alert-success"> -->
                    <div class="jumbotron">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h1 class="display-3">{{ $data['message'] }}</h1>
                                @if ($data['status'] === 200)
                                <small>Payment Id</small>
                                <h5 class="display-3">{{ json_decode($data['response'], true)['paymentDataId'] }}</h5>

                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <img src="img/success-icon4.gif" width="200" height="200" class="picture-src" title="" />
                                    </div>
                                </div>
                                @else
                                <small>Error Message is</small>
                                <h5 class="display-3 text-danger">{{ json_decode($data['response'], true)['error'] }}</h5>
                                @endif
                            </div>
                        </div>

                        
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button type="button" class="btn btn-lg btn-info mt-4" onclick="window.location='{{ url("/") }}'">Go back to the registration page</button>
                        </div>
                    </div>

                @endif

                @if (!isset($data))
                <!-- Wizard container  -->
                <div class="wizard-container">
                    <div class="card wizard-card" data-color="blue" id="wizardProfile">
                        <form action="/store" method="POST">
                            <!-- You can switch " data-color="orange" " with one of the next bright colors: "blue", "green", "orange", "red", "azure" -->
                            <div class="wizard-header text-center">
                                <h3 class="wizard-title">User registration</h3>
                                <p class="category">This information will let us know more about you.</p>
                            </div>

                            <div class="wizard-navigation">
                                <div class="progress-with-circle">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="3" style="width: 21%;"></div>
                                </div>
                                <ul>
                                    <li>
                                        <a href="#about" data-toggle="tab">
                                            <div class="icon-circle">
                                                <i class="ti-user"></i>
                                            </div>
                                            Personal Information
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#address" data-toggle="tab">
                                            <div class="icon-circle">
                                                <i class="ti-map"></i>
                                            </div>
                                            Address Details
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#payment" data-toggle="tab">
                                            <div class="icon-circle">
                                                <i class="ti-credit-card"></i>
                                            </div>
                                            Payment Method
                                        </a>
                                    </li>

                                </ul>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane" id="about">
                                    <div class="row">
                                        <h5 class="info-text"> Please tell us more about yourself.</h5>
                                        <div class="col-sm-5 col-sm-offset-1">
                                            <div class="form-group">
                                                <label>First Name <small>(required)</small></label>
                                                <input name="firstname" type="text" class="form-control" placeholder="John" value="John">
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <label>Last Name <small>(required)</small></label>
                                                <input name="lastname" type="text" class="form-control" placeholder="Doe" value="Doe">
                                            </div>
                                        </div>

                                        <div class="col-sm-5 col-sm-offset-1">
                                            <div class="form-group">
                                                <label>Telephone <small>(required)</small></label>
                                                <input name="telephone" type="text" class="form-control" placeholder="+00 0000000000" value="+00 0000000000">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="address">
                                    <h5 class="info-text"> Could you please confirm your address? </h5>
                                    <div class="row">
                                        <div class="col-sm-10 col-sm-offset-1">
                                            <div class="form-group">
                                                <label>Address (Including Street Name, House Number, Zip Code, City)</label>
                                                <input name="address" type="text" class="form-control" placeholder="Hongkongstraße 2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="payment">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h5 class="info-text"> Please inform the payment details </h5>
                                        </div>
                                        <div class="col-sm-7 col-sm-offset-1">
                                            <div class="form-group">
                                                <label>Account Owner</label>
                                                <input name="account_owner" type="text" class="form-control" placeholder="John Doe">
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>IBAN</label>
                                                <input name="iban" type="text" class="form-control" placeholder="DE8234">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wizard-footer">
                                <div class="pull-right">
                                    <input type='button' class='btn btn-next btn-fill btn-info btn-wd' name='next' value='Next' />
                                    <input type='submit' class='btn btn-finish btn-fill btn-warning btn-wd' name='finish' value='Finish' />
                                </div>

                                <div class="pull-left">
                                    <input type='button' class='btn btn-previous btn-default btn-wd' name='previous' value='Previous' />
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>
                </div> <!-- wizard container -->
                @endif

            </div>
        </div>

    </body>

    <!--   Core JS Files   -->
    <script src="js/jquery-2.2.4.min.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/jquery.bootstrap.wizard.js" type="text/javascript"></script>

	<!--  Plugin for the Wizard -->
	<script src="js/demo.js" type="text/javascript"></script>
	<script src="js/paper-bootstrap-wizard.js" type="text/javascript"></script>

	<!--  More information about jquery.validate here: https://jqueryvalidation.org/ -->
	<script src="js/jquery.validate.min.js" type="text/javascript"></script>

</html>