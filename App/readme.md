## About This Technical Project

This technical test was developed using the following technologies:

- docker-composer
- Laravel 5.8.*
- PHP 7.4.0
- MySQL 5.7.27

## Running the docker container

Please guarantee that docker is installed
https://docs.docker.com/docker-for-windows/

Intialize the container:
    docker-compose up --build -d

Assuming everything is okay, you can see the project up and running. Also you can access the images in case necessary:
    docker exec -it wunder_web bash
    docker exec -it wunder_php bash

    From within the App folder, please apply the following permission to the storage folder:
        sudo chmod 777 -R storage/
    
    You may need to access wunder_php container, cd App/ and then execute the following commands:
        php artisan config:clear
        php artisan cache:clear


## About the database

    It is necessary to run the migration file to be able to see the table:

        docker exec -it wunder_php bash
        cd App/
        php artisan migrate --path=database/migrations/2020_02_06_194438_create_user_registration_table.php

    Or you can access the database and run the create table command:

        CREATE TABLE `user_registration` (
        `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        `firstname` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
        `lastname` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
        `telephone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
        `address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
        `account_owner` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
        `iban` varchar(34) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        `statusCode` smallint(5) unsigned DEFAULT NULL,
        `status` enum('Pending','Error','Paid','Rejected','Not Processed') COLLATE utf8mb4_unicode_ci NOT NULL,
        `response` text COLLATE utf8mb4_unicode_ci,
        `created_at` timestamp NULL DEFAULT NULL,
        `updated_at` timestamp NULL DEFAULT NULL,
        PRIMARY KEY (`id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;    


## Describing possible performance optimizations

I would recommend split the user_registration table into two:
    one table to keep the user login
    and one table to keep all the transactions made by the user.

Make use of Trait or ServiceProviders would be interesting to abstract even more the payment methods.

I would strongly recommend the use of a Non-SQL database such as MongoDB, as the pipeline is really interesting to process data (rollup, mapReduce and cube) to produce reports and then improve Customer Experience. MongoDB has an important feature called GeoSpatial. Also adding transactions to the users collection which accepts up to 16MB would increase the performance exponentially.

Create tables to maintain routes, ips and endpoints (verb, method) to improve security from where the endpoints are being used. So, in this way we can control each payment machine and the services they can use in case applicable. If the company allow third-party to process payments, then these new tables would fit.

Frontend could be separated from backend, so we could have an Angular Application calling routes via ServiceProviders to the backend, then passing the credentials initially and getting a token. Each time FE calls to backend validates the token and then if valid proceed. Backend returns with a json response to FE.

## Which things could be done better, than you've done it?
    I could have abstracted more the Payment Controller Methods.
    Created a separate page to handle the responses
    Implemented a proper login page and then allow registered users to make payments

