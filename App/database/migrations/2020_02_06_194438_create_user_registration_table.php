<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRegistrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_registration', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstname', 30);
            $table->string('lastname', 30);
            $table->string('telephone', 20);
            $table->string('address', 100);
            $table->string('account_owner', 60);
            $table->string('iban', 34)->nullable(); // accepting iban equals null for test purpose, so you can test handling errors
            $table->unsignedSmallInteger('statusCode')->nullable();
            $table->enum('status', ['Pending', 'Error', 'Paid', 'Rejected', 'Not Processed']);
            $table->text('response', 300)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_registration');
    }
}
