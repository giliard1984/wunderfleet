<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UserRegistration;

class UserRegistrationController extends Controller
{
    // This is the main method called to render the initial page
    public function index($data = null) 
    {
        return view('wunder', ['data' => $data]);
    }

    // The following method validates the data sent and if validate stores it against the database
    public function store(Request $request)
    {
        // validate if required fields were submitted accordingly 
        $validatedData = $request->validate([
            'firstname' => 'required|max:30',
            'lastname' => 'required|max:30',
            'telephone' => 'required|max:20',
            'address' => 'required|max:100',
            'account_owner' => 'required|max:60'
            // 'iban' => 'required|max:34'
        ]);

        // mounts the data to be stored
        $data = [
            'firstname' => $request->get('firstname'),
            'lastname' => $request->get('lastname'),
            'telephone' => $request->get('telephone'),
            'address' => $request->get('address'),
            'account_owner' => $request->get('account_owner'),
            'iban' => $request->get('iban'),
            'statusCode' => null,
            'status' => 'Pending',
            'response' => null
        ];

        // calls the create method from eloquent which saves the data
        $user = UserRegistration::create($data);

        // gets the id generated by the method above to be used as part of the body to be posted on to the endpoint
        $data['client_number_id'] = $user->id;

        // make the payment and gets the results
        $payment_result = self::make_payment($data);

        // considering the post method worked, the reads the status code and messages accordingly to provide an adequately message to users
        if (!empty($payment_result['statusCode'])) {

            $payment_result['statusCode'] === 200 ? $user->status = "Paid" : $user->status = "Error";
            $user->statusCode = $payment_result['statusCode'];
            $user->response = json_encode($payment_result['response']);

            $user->save();

        } else {

            $user->status = "Not Processed";
            $user->save();

        }

        // run a simple check to prepare the message to displayed on the screen
        $message = $payment_result['statusCode'] === 200 ?  "Registration process has been successfully completed" : 'Sorry! Something went wrong! Could you please try again?';

        // passes the values to the main method to render messages
        return self::index($data = ['message' => $message, 'status' => $payment_result['statusCode'], 'response' => json_encode($payment_result['response'])]);

    }

    // method to make payment
    private function make_payment($data) 
    {
        $client = new \GuzzleHttp\Client([
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ]
        ]);

        $url = "https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data";

        $data = [
            "customerId" => $data['client_number_id'],
            "iban" => $data['iban'],
            "owner" => $data['account_owner']
        ];

        try {

            $request = $client->request('POST',$url, ['json' => $data]);
            $code = $request->getStatusCode();
            $response = $request->getBody()->getContents();
            
        } catch (\Exception $e) {  //handles errors

            $response = $e->getResponse()->getBody()->getContents();
            $code = $e->getCode();

        }

        return [
            'statusCode' => $code,
            'response' => json_decode($response, true)
        ];

    }
}