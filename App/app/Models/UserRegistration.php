<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRegistration extends Model
{
    // express the table to be used
    protected $table = 'user_registration';

    protected $fillable = array('firstname', 'lastname', 'telephone', 'address', 'account_owner', 'iban', 'statusCode', 'status', 'response');

    public $timestamps = true;

}
